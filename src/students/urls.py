"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from .views import (
    AllStudentsView,
    CreateStudentView,
    UpdateStudentView,
    DeleteStudentView,
    SearchHistoryView,
    FindStudentsView,
)

app_name = "students"

urlpatterns = [
    path("", AllStudentsView.as_view(), name="all_students"),
    path("create", CreateStudentView.as_view(), name="create_students"),
    path("find", FindStudentsView.as_view(), name="find_students"),
    path("update/<uuid:uuid>", UpdateStudentView.as_view(), name="update_students"),
    path("delete/<uuid:uuid>", DeleteStudentView.as_view(), name="delete_students"),
    path("search-history", SearchHistoryView.as_view(), name="search_history"),
]
