from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from django.forms import ModelForm
from django import forms

from .models import Students


class GetIDForm(forms.Form):
    Student_ID = forms.IntegerField(min_value=0)


class FindStudent(forms.Form):
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    search_text = forms.CharField(required=False)

    def clean_first_name(self):
        return self.cleaned_data["first_name"]

    def clean_last_name(self):
        return self.cleaned_data["last_name"]

    def clean_search_text(self):
        return self.cleaned_data["search_text"]


class StudentForm(ModelForm):
    class Meta:
        model = Students
        fields = ("first_name", "last_name", "email", "birth_date", "grade", "group", "avatar", "resume")

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_email(self):
        email = self.cleaned_data["email"]
        if "@yandex" in email.lower():
            raise ValidationError("Yandex domain is forbidden in our country")
        return email

    def clean_first_name(self):
        return self.normalize_text(self.cleaned_data["first_name"])

    def clean_last_name(self):
        return self.normalize_text(self.cleaned_data["last_name"])

    def clean(self):
        cleaned_data = super().clean()

        first_name = cleaned_data["first_name"]
        last_name = cleaned_data["last_name"]

        if first_name == last_name:
            raise ValidationError("First name and last name can`t be equal")

        return cleaned_data

    def clean_avatar(self):
        avatar = self.cleaned_data["avatar"]
        if not self.instance.avatar == avatar:
            try:
                w, h = get_image_dimensions(avatar)

                # validate dimensions
                max_width = max_height = 200
                if w > max_width or h > max_height:
                    raise forms.ValidationError(
                        "Please use an image that is " "%s x %s pixels or smaller." % (max_width, max_height)
                    )

                # validate file size
                if len(avatar) > (20 * 1024):
                    raise forms.ValidationError("Avatar file size may not exceed 20k.")

            except AttributeError:
                """
                Handles case when we are updating the user profile
                and do not supply a new avatar
                """
                pass

        return avatar
