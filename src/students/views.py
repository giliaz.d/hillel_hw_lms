from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, ListView, FormView
from datetime import datetime

from .models import Students
from .forms import StudentForm, FindStudent


class IndexView(TemplateView):
    template_name = "index.html"
    extra_context = {"title": "LMS Home page"}


class AllStudentsView(ListView):
    model = Students
    template_name = "students/list.html"
    context_object_name = "students"
    extra_context = {"title": "Students list"}


class FindStudentsView(FormView):
    queryset = Students.objects.all()
    form_class = FindStudent
    template_name = "find_record.html"
    success_url = reverse_lazy("students:all_students")

    def get(self, request, *args, **kwargs):
        students = Students.objects.all()
        my_kwargs = {
            "first_name": request.GET.get("first_name"),
            "last_name": request.GET.get("last_name"),
            "search_text": request.GET.get("search_text"),
        }
        search_field = ["first_name", "last_name", "email"]

        for param_name, param_val in my_kwargs.items():
            if param_val:
                if param_name == "search_text":
                    request.session[f"search_text_{datetime.now()}"] = param_val
                    or_filter = Q()
                    for field in search_field:
                        or_filter |= Q(**{f"{field}__icontains": param_val})
                    students = students.filter(or_filter)
                    return render(
                        request,
                        template_name="students/list.html",
                        context={"students": students, "title": "Found students"},
                    )
                else:
                    request.session[f"search_name_{datetime.now()}"] = param_val
                    students = students.filter(**{param_name: param_val})
                    return render(
                        request,
                        template_name="students/list.html",
                        context={"students": students, "title": "Found students"},
                    )

        return render(
            request,
            template_name="find_record.html",
            context={"form": FindStudent, "name": "student", "title": "Find student"},
        )


class CreateStudentView(CreateView):
    http_method_names = ["get", "post"]
    model = Students
    form_class = StudentForm
    template_name = "create_record.html"
    extra_context = {"name": "student", "title": "Create student"}
    success_url = reverse_lazy("students:all_students")


class UpdateStudentView(UpdateView):
    queryset = Students.objects.all()
    model = Students
    form_class = StudentForm
    pk_url_kwarg = "uuid"
    template_name = "update_record.html"
    extra_context = {"name": "student", "title": "Edit student"}
    success_url = reverse_lazy("students:all_students")


class DeleteStudentView(DeleteView):
    http_method_names = ["get", "post"]
    model = Students
    pk_url_kwarg = "uuid"
    template_name = "delete_record.html"
    extra_context = {"name": "student", "title": "Delete student"}
    success_url = reverse_lazy("students:all_students")

    # delete without form validation
    # def get(self, request, *args, **kwargs):
    #     student = get_object_or_404(Students.objects.all(), pk=kwargs['uuid'])
    #     student.delete()
    #     return HttpResponseRedirect(reverse("students:all_students"))


class SearchHistoryView(TemplateView):
    http_method_names = ["get"]
    template_name = "search_history.html"

    def get(self, request, *args, **kwargs):
        log_list = {}
        for key, value in request.session.items():
            if ("search_name" in key) or ("search_text" in key):
                log_list.update({key: value})
        return render(
            request, template_name="search_history.html", context={"search_logs": log_list, "title": "Request history"}
        )
