from django.core.exceptions import ValidationError


# formatters output date
def format_records(all_students) -> str:
    result = ""
    output_fields = ["id", "first_name", "last_name", "email", "birth_date", "age", "grade"]
    for data_one_student in all_students.values():
        list_output_fields = [str(values) for key, values in data_one_student.items() if key in output_fields]
        result += " ".join(list_output_fields) + "<br>"
    return result
    # return "<br>".join(str(line) for line in all_students)


# validators of fields
def first_name_validator(first_name: str) -> None:
    if "vova" in first_name.lower():
        raise ValidationError("Vova is not correct name, should be Volodymyr")
