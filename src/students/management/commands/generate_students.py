from django.core.management.base import BaseCommand

from faker import Faker

faker_instance = Faker("EN")


def generate_students(num):
    person = [
        [
            faker_instance.first_name(),
            faker_instance.last_name(),
            faker_instance.email(),
            faker_instance.password(),
            faker_instance.date_of_birth(minimum_age=20, maximum_age=70).strftime("%m.%d.%Y"),
        ]
        for _ in range(num)
    ]

    return person


class Command(BaseCommand):
    help = "Displays list names of students"

    def add_arguments(self, parser):
        parser.add_argument("num", type=int, help="Enter amount of students")

    def handle(self, *args, **kwargs):
        num = kwargs["num"]

        list_of_students = ""
        for line in generate_students(num):
            list_of_students += line[0] + "\n"

        self.stdout.write(list_of_students)
