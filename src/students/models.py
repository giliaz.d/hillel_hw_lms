from uuid import uuid4

from django.core.validators import (
    MinLengthValidator,
    MaxValueValidator,
    MinValueValidator,
    FileExtensionValidator,
)
from django.db import models

from datetime import datetime

from django.utils import timezone
from faker import Faker

from .format_records import first_name_validator

from groups.models import Groups


class Students(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid4,
        unique=True,
        db_index=True,
    )

    first_name = models.CharField(
        max_length=100,
        default="",
        null=True,
        validators=[MinLengthValidator(2), first_name_validator],
    )
    last_name = models.CharField(
        max_length=100,
        default="",
        null=True,
        validators=[
            MinLengthValidator(2),
        ],
    )
    email = models.EmailField(max_length=100, default="uncnown@unknown.com", null=False)
    birth_date = models.DateField(default=timezone.now, null=True)
    grade = models.PositiveSmallIntegerField(
        default=0, null=True, validators=[MaxValueValidator(100), MinValueValidator(0)]
    )
    group = models.ForeignKey(
        to=Groups, on_delete=models.CASCADE, default=None, blank=True, null=True, related_name="students"
    )

    def __str__(self):
        return (
            f"{self.pk} {self.first_name} {self.last_name} {self.email} "
            f"{self.birth_date.strftime('%d.%m.%Y')} {self.age()} {self.grade} "
        )

    def age(self):
        return datetime.now().year - self.birth_date.year

    avatar = models.ImageField(
        upload_to="images", default="../media/ava.png", validators=[FileExtensionValidator(["png", "jpeg", "gif"])]
    )
    resume = models.FileField(upload_to="files", blank=True, validators=[FileExtensionValidator(["pdf", "docx"])])

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-40y", end_date="-18y"),
            )
