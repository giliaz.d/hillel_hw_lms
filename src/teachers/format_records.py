from django.core.exceptions import ValidationError


# formatters output date
def format_records(all_teachers) -> str:
    result = ""
    output_fields = [
        "id",
        "first_name",
        "last_name",
        "email",
        "phone_number",
        "profession",
    ]
    for data_one_teachers in all_teachers.values():
        list_output_fields = [str(values) for key, values in data_one_teachers.items() if key in output_fields]
        result += " ".join(list_output_fields) + "<br>"
    return result
    # return "<br>".join(str(line) for line in all_teachers)


# validators of fields
def profession_validator(profession: str) -> None:
    if profession.isdigit():
        raise ValidationError("The profession must be filled with letters")
