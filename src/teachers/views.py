from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, FormView, CreateView, UpdateView, DeleteView

from .forms import TeacherForm, FindTeacher
from .models import Teachers


class AllTeachersView(ListView):
    model = Teachers
    template_name = "teachers/list.html"
    context_object_name = "teachers"
    extra_context = {"title": "Teachers list"}


class FindTeachersView(FormView):
    queryset = Teachers.objects.all()
    form_class = FindTeacher
    template_name = "find_record.html"
    success_url = reverse_lazy("teachers:all_teachers")

    def get(self, request, *args, **kwargs):
        teachers = Teachers.objects.all()
        my_kwargs = {
            "first_name": request.GET.get("first_name"),
            "last_name": request.GET.get("last_name"),
            "search_text": request.GET.get("search_text"),
        }
        search_field = ["first_name", "last_name", "email"]

        for param_name, param_val in my_kwargs.items():
            if param_val:
                if param_name == "search_text":
                    or_filter = Q()
                    for field in search_field:
                        or_filter |= Q(**{f"{field}__icontains": param_val})
                    teachers = teachers.filter(or_filter)
                    return render(
                        request,
                        template_name="teachers/list.html",
                        context={"teachers": teachers, "title": "Found teachers"},
                    )
                else:
                    teachers = teachers.filter(**{param_name: param_val})
                    return render(
                        request,
                        template_name="teachers/list.html",
                        context={"teachers": teachers, "title": "Found teachers"},
                    )

        return render(
            request,
            template_name="find_record.html",
            context={"form": FindTeacher, "name": "teachers", "title": "Find teachers"},
        )


class CreateTeacherView(CreateView):
    http_method_names = ["get", "post"]
    model = Teachers
    form_class = TeacherForm
    template_name = "create_record.html"
    extra_context = {"name": "teacher", "title": "Create teacher"}
    success_url = reverse_lazy("teachers:all_teachers")


class UpdateTeacherView(UpdateView):
    queryset = Teachers.objects.all()
    model = Teachers
    form_class = TeacherForm
    pk_url_kwarg = "pk"
    template_name = "update_record.html"
    extra_context = {"name": "teacher", "title": "Edit teacher"}
    success_url = reverse_lazy("teachers:all_teachers")


class DeleteTeacherView(DeleteView):
    http_method_names = ["get", "post"]
    model = Teachers
    pk_url_kwarg = "pk"
    template_name = "delete_record.html"
    extra_context = {"name": "teacher", "title": "Delete teacher"}
    success_url = reverse_lazy("teachers:all_teachers")
