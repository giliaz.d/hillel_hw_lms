from django.core.validators import MinLengthValidator, RegexValidator, FileExtensionValidator
from django.db import models

from datetime import datetime

from django.utils import timezone
from faker import Faker

from .format_records import profession_validator
from groups.models import Groups

phoneNumberRegex = RegexValidator(regex=r"[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$")


class Teachers(models.Model):
    first_name = models.CharField(
        max_length=100,
        default="",
        null=True,
        validators=[
            MinLengthValidator(2),
        ],
    )
    last_name = models.CharField(
        max_length=100,
        default="",
        null=True,
        validators=[
            MinLengthValidator(2),
        ],
    )
    email = models.EmailField(max_length=100, default="email@unknown.com", null=False)
    birth_date = models.DateField(default=timezone.now, null=True)
    phone_number = models.CharField(default="000-000-00-00", validators=[phoneNumberRegex], max_length=15, null=True)
    profession = models.CharField(
        max_length=100,
        default="",
        null=True,
        validators=[MinLengthValidator(2), profession_validator],
    )
    group = models.ManyToManyField(to=Groups, related_name="teachers", blank=True)

    def __str__(self):
        return (
            f"{self.pk} {self.first_name} {self.last_name} {self.birth_date.strftime('%m.%d.%Y')}"
            f" {self.phone_number} {self.profession}"
        )

    def age(self):
        return datetime.now().year - self.birth_date.year

    avatar = models.ImageField(
        upload_to="images", default="../media/ava.png", validators=[FileExtensionValidator(["png", "jpeg", "gif"])]
    )

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-50y", end_date="-30y"),
                phone_number=faker.phone_number(),
                profession=faker.job(),
            )
