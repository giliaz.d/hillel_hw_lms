from django.contrib import admin  # NOQA

# Register your models here.
from students.models import Students
from teachers.models import Teachers
from groups.models import Groups
from users.models import Profile, RelatedTo


admin.site.register([Profile, RelatedTo, Students, Teachers, Groups])
