from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django import forms


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = get_user_model()
        fields = ("username", "first_name", "last_name", "email", "password1", "password2")


class ChangePasswordForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ("password", "password1", "password2")


class SendEmailForm(forms.Form):
    email = forms.EmailField(max_length=50)
    text = forms.CharField(widget=forms.Textarea)
