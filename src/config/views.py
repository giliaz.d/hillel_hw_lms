from datetime import datetime
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail
from django.shortcuts import render
from django.contrib.auth import login, get_user_model
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView, PasswordChangeView
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import CreateView, RedirectView

from .forms import RegistrationForm, SendEmailForm
from django.conf import settings

from services.send_registration_email import send_registration_email
from services.token_generator import TokenGenerator


class Login(LoginView):
    extra_context = {"title": "Login"}
    pass


class Logout(LogoutView):
    pass


# ------------------------- registration---------------------------
class Registration(CreateView):
    template_name = "registration/create_user.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("index")
    extra_context = {"title": "Registration"}

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        send_registration_email(request=self.request, user_instance=self.object)
        return super().form_valid(form)


class ActivateUser(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return render(
                request,
                template_name="registration/wrong_registration.html",
                context={"form": "(exception 'user does not exist or type/value error')", "title": "Error"},
            )

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()
            login(request, current_user)
            return super().get(request, *args, **kwargs)

        return render(
            request,
            template_name="registration/wrong_registration.html",
            context={"form": "(error 'expired time or wrong email/token')", "title": "Error"},
        )


# -----------------------------end of registration--------------------------------


class ResetPasswordView(SuccessMessageMixin, PasswordResetView):
    template_name = "registration/reset_password/password_reset.html"
    email_template_name = "registration/reset_password/password_reset_email.html"
    subject_template_name = "registration/reset_password/password_reset_subject.txt"
    success_message = (
        "We've emailed you instructions for setting your password, "
        "if an account exists with the email you entered. You should receive them shortly."
        " If you don't receive an email, "
        "please make sure you've entered the address you registered with, and check your spam folder."
    )
    success_url = reverse_lazy("index")


class ChangePasswordView(SuccessMessageMixin, PasswordChangeView):
    template_name = "registration/change_password/change_password_2.html"
    success_message = "Successfully Changed Your Password"
    success_url = reverse_lazy("index")


def send_email(request):
    if request.method == "POST":
        form = SendEmailForm(request.POST)
        if form.is_valid():
            user = get_user_model().objects.filter(email=f'{request.POST.get("email")}')
            if user:
                email_from = request.POST.get("email")
                text = request.POST.get("text")
                subject = "Send email to administrator LMS"
                message = (
                    f"{datetime.now().strftime('%d %B %Y %H:%M')} from user '{user[0]}', {email_from}.\n\n"
                    f" {text}.\n"
                    f"\nThis email was generated automatically, please do not reply to it."
                )
                recipient_list = [settings.EMAIL_HOST_USER, email_from]
                send_mail(subject, message, email_from, recipient_list)
                return HttpResponseRedirect(reverse("index"))
            else:
                form.add_error("email", "The user does not exist, register first!")
        else:
            form.add_error("email", "Please fill all fields correctly!")
            return render(request, "emails/email_from_user.html", {"form": form, "title": "Send email"})
    else:
        form = SendEmailForm()
    return render(request, "emails/email_from_user.html", {"form": form, "title": "Send email"})


def page_not_found_view(request, exception):
    return render(request, "404.html", status=404)


# def change_password(request):
#     user_1 = User.objects.get(username=request.user)
#     if request.method == "POST":
#         form = ChangePasswordForm(request.POST)
#         if form.is_valid():
#             old_password = request.POST.get("password")
#             new_pass = request.POST.get("password1")
#             new_pass_rep = request.POST.get("password2")
#             if check_password(old_password, user_1.password):
#                 if new_pass == new_pass_rep:
#                     user_1.password = make_password(new_pass, salt=None, hasher="default")
#                     user_1.save()
#                     login(request, user_1)
#                     return HttpResponseRedirect(reverse("index"))
#             else:
#                 form.add_error("password", "Password is not valid!")
#                 render(
#                     request,
#                     "registration/change_password/change_password.html",
#                     {"form": form, "user": user_1, "title": "Change password"},
#                 )
#     else:
#         form = ChangePasswordForm()
#
#     return render(
#         request,
#         "registration/change_password/change_password.html",
#         {"form": form, "user": user_1, "title": "Change password"}
#     )
