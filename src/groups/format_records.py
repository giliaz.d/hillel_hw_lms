from django.core.exceptions import ValidationError


# formatters output date
def format_records(all_groups) -> str:
    return "<br>".join(str(line) for line in all_groups)


# validators of fields
def subject_validator(subject: str) -> None:
    if subject.isdigit():
        raise ValidationError("The subject must be filled with letters")
