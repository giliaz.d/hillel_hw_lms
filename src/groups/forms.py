from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django import forms

from .models import Groups


class Get_ID_Form(forms.Form):
    Groups_ID = forms.IntegerField(min_value=0)


class FindGroup(forms.Form):
    subject = forms.CharField(required=False)
    class_mentor = forms.CharField(required=False)
    search_text = forms.CharField(required=False)


class GroupForm(ModelForm):
    class Meta:
        model = Groups
        fields = (
            "subject",
            "class_mentor",
            "email",
            "start_of_study_date",
            "end_of_study_date",
            "group_average_grade",
        )

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email.lower():
            raise ValidationError("Yandex domain is forbidden in our country")

        return email

    def clean_subject(self):
        return self.normalize_text(self.cleaned_data["subject"])

    def clean_class_mentor(self):
        return self.cleaned_data["class_mentor"]

    def clean(self):
        cleaned_data = super().clean()
        return cleaned_data
