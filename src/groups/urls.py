"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from .views import (
    AllGroupsView,
    FindGroupsView,
    CreateGroupView,
    UpdateGroupView,
    DeleteGroupView,
    ShowAnyGroupsView,
)


app_name = "groups"

urlpatterns = [
    path("", AllGroupsView.as_view(), name="all_groups"),
    path("find", FindGroupsView.as_view(), name="find_groups"),
    path("create", CreateGroupView.as_view(), name="create_groups"),
    path("update/<int:pk>", UpdateGroupView.as_view(), name="update_groups"),
    path("delete/<int:pk>", DeleteGroupView.as_view(), name="delete_groups"),
    path("show/<int:pk>", ShowAnyGroupsView.as_view(), name="show_any_groups"),
]
