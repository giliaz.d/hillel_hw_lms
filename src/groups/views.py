from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView, FormView

from .forms import GroupForm, FindGroup
from .models import Groups
from teachers.models import Teachers


class AllGroupsView(ListView):
    model = Groups
    template_name = "groups/list.html"
    context_object_name = "groups"
    extra_context = {"title": "Groups list"}


class FindGroupsView(FormView):
    queryset = Groups.objects.all()
    form_class = FindGroup
    template_name = "find_record.html"
    success_url = reverse_lazy("groups:all_groups")

    def get(self, request, *args, **kwargs):
        groups = Groups.objects.all()
        my_kwargs = {
            "subject": request.GET.get("subject"),
            "class_mentor": request.GET.get("class_mentor"),
            "search_text": request.GET.get("search_text"),
        }
        search_field = ["subject", "class_mentor", "email"]

        for param_name, param_val in my_kwargs.items():
            if param_val:
                if param_name == "search_text":
                    or_filter = Q()
                    for field in search_field:
                        or_filter |= Q(**{f"{field}__icontains": param_val})
                    groups = groups.filter(or_filter)
                    return render(
                        request,
                        template_name="groups/list.html",
                        context={"groups": groups, "title": "Groups list"},
                    )
                else:
                    groups = groups.filter(**{param_name: param_val})
                    return render(
                        request,
                        template_name="groups/list.html",
                        context={"groups": groups, "title": "Groups list"},
                    )

        return render(
            request,
            template_name="find_record.html",
            context={"form": FindGroup, "name": "group", "title": "Find group"},
        )


class CreateGroupView(CreateView):
    http_method_names = ["get", "post"]
    model = Groups
    form_class = GroupForm
    template_name = "create_record.html"
    extra_context = {"name": "group", "title": "Create group"}
    success_url = reverse_lazy("groups:all_groups")


class UpdateGroupView(UpdateView):
    queryset = Groups.objects.all()
    model = Groups
    form_class = GroupForm
    pk_url_kwarg = "pk"
    template_name = "update_record.html"
    extra_context = {"name": "group", "title": "Edit group"}
    success_url = reverse_lazy("groups:all_groups")


class DeleteGroupView(DeleteView):
    http_method_names = ["get", "post"]
    model = Groups
    pk_url_kwarg = "pk"
    template_name = "delete_record.html"
    extra_context = {"name": "group", "title": "Delete group"}
    success_url = reverse_lazy("groups:all_groups")


class ShowAnyGroupsView(ListView):
    model = Groups
    template_name = "groups/list.html"
    context_object_name = "groups"
    extra_context = {"title": "Groups list"}

    def get_queryset(self):
        queryset = Teachers.objects.filter(pk=self.kwargs["pk"])[0].group.all()
        return queryset
