from django.core.validators import (
    MinLengthValidator,
    MaxValueValidator,
    MinValueValidator,
)
from django.db import models

import random

from django.utils import timezone
from faker import Faker

from .format_records import subject_validator


class Groups(models.Model):
    subject = models.CharField(
        max_length=100,
        default="",
        null=True,
        validators=[MinLengthValidator(2), subject_validator],
    )
    start_of_study_date = models.DateField(default=timezone.now, null=True)
    end_of_study_date = models.DateField(default=timezone.now, null=True)
    group_average_grade = models.PositiveSmallIntegerField(
        default=0, null=True, validators=[MinValueValidator(0), MaxValueValidator(100)]
    )
    class_mentor = models.CharField(
        max_length=100,
        default="",
        null=True,
        validators=[
            MinLengthValidator(2),
        ],
    )
    email = models.EmailField(max_length=100, default="email@unknown.com", null=False)

    def __str__(self):
        return f"{self.pk} {self.subject}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                subject=faker.job(),
                start_of_study_date=faker.date_time_between(start_date="-10y", end_date="-5y"),
                end_of_study_date=faker.date_time_between(start_date="-5y", end_date="-1y"),
                group_average_grade=random.randint(70, 100),
                class_mentor=faker.name(),
                email=faker.email(),
            )
