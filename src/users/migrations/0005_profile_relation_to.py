# Generated by Django 4.1.1 on 2022-09-21 01:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("users", "0004_remove_profile_relation_to_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="profile",
            name="relation_to",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="profile",
                to="users.relatedto",
            ),
        ),
    ]
