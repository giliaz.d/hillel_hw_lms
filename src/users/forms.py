from django import forms
from django.contrib.auth import get_user_model
from .models import Profile


class UpdateUserForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ["first_name", "last_name", "email"]


class UpdateProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["birth_date", "phone_number", "city", "relation_to", "photo", "resume", "notes"]
