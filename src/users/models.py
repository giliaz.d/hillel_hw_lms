from django.core.validators import FileExtensionValidator, RegexValidator
from django.db import models
from django.utils import timezone
from django.conf import settings
from PIL import Image

from location_field.models.plain import PlainLocationField

phoneNumberRegex = RegexValidator(regex=r"[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$")


# class CustomUser(AbstractUser, PermissionsMixin):
#     username = None
#     email = models.EmailField(_('email address'), unique=True)
#
#     USERNAME_FIELD = 'email'
#     REQUIRED_FIELDS = []
#
#     objects = CustomUserManager()
#
#     def __str__(self):
#         return self.email


class RelatedTo(models.Model):
    relation_to = models.CharField(
        max_length=20,
        default="",
        null=False,
    )

    def __str__(self):
        return f"{self.relation_to}"


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    birth_date = models.DateField(default=timezone.now, blank=True)
    phone_number = models.CharField(default="000-000-00-00", validators=[phoneNumberRegex], max_length=15, blank=True)
    city = models.CharField(max_length=100, default="", blank=True)
    location = PlainLocationField(based_fields=["city"], zoom=7)
    photo = models.ImageField(
        upload_to="images",
        default="../media/ava.png",
        blank=True,
        validators=[FileExtensionValidator(["png", "jpg", "jpeg", "gif"])],
    )
    resume = models.FileField(upload_to="files", blank=True, validators=[FileExtensionValidator(["pdf", "docx"])])
    notes = models.TextField(
        max_length=100,
        default="",
        blank=True,
    )
    relation_to = models.ForeignKey(
        to=RelatedTo, on_delete=models.CASCADE, default=None, blank=True, null=True, related_name="profile"
    )

    def __str__(self):
        return self.user.username

    # resizing images
    def save(self, *args, **kwargs):
        super().save()
        img = Image.open(self.photo.path)
        if img.height > 300 or img.width > 300:
            new_img = (300, 300)
            img.thumbnail(new_img)
            img.save(self.photo.path)
